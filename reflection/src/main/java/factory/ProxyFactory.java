package factory;

import annotation.Proxy;

import java.lang.reflect.InvocationHandler;

/**
 * Factory for for proxy beans
 */
public class ProxyFactory {

    /**
     * Get instance of proxy or simple instance
     * @param objectClass - bean class
     * @return - instance or proxy
     */
    public static final Object getInstanceOf(Class objectClass) throws ReflectiveOperationException {
        Object instance = objectClass.newInstance();
        if(objectClass.isAnnotationPresent(Proxy.class)){
            String invocationHandlerClassName = ((Proxy) objectClass.getAnnotation(Proxy.class)).invocationHandler();
            InvocationHandler handler = (InvocationHandler)Class.forName(invocationHandlerClassName).getConstructor(Object.class).newInstance(instance);
            instance = java.lang.reflect.Proxy.newProxyInstance(objectClass.getClassLoader(), objectClass.getInterfaces(), handler);
        }
        return instance;
    }
}
