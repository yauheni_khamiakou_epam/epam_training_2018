package bean;

import java.util.Optional;

/**
 * Animal's interface
 */
public interface IAnimal {
    /**
     * @param name to set
     */
    public void setName(String name);

    /**
     * @return animal's name
     */
    public Optional<String> getName();

    /**
     * @param name
     */
    public void rename(String name);
}
