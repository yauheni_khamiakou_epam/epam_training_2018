package bean;

import annotation.Equal;
import annotation.EqualType;

import java.util.Objects;
import java.util.Optional;

/**
 * No-proxy cat bean
 */
public class Cat implements IAnimal {
    /**
     * Fields
     */
    @Equal(compare = EqualType.REFERENCE)
    private String name;
    @Equal(compare = EqualType.VALUE)
    private Integer age;

    /**
     * Args constructor
     * @param name - cat's name
     * @param age - cat's age
     */
    public Cat(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    /**
     * @return cat's age
     */
    public Integer getAge() {
        return age;
    }

    /**
     * @param age for cat to set
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Optional<String> getName() {
        return Optional.ofNullable(this.name);
    }

    @Override
    public void rename(String name) {
        if(!name.equals(this.name)){
            this.name = name;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cat cat = (Cat) o;
        return Objects.equals(name, cat.name) &&
                Objects.equals(age, cat.age);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, age);
    }
}
