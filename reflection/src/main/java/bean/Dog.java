package bean;

import annotation.Proxy;

import java.util.Optional;

/**
 * Proxy dog bean
 */
@Proxy(invocationHandler = "handler.LoggingHandler")
public class Dog implements IAnimal{
    /**
     * Fields
     */
    private String name;

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Optional<String> getName() {
        return Optional.ofNullable(this.name);
    }

    @Override
    public void rename(String name) {
        if(!name.equals(this.name)){
            this.name = name;
        }
    }
}
