package equal;

import annotation.Equal;

import java.lang.reflect.Field;

/**
 * Utility class for equaling objects
 */
public class EqualAnalyzer {

    /**
     * Equal two objects
     * @param object1 - first object to equal
     * @param object2 - second object to equal
     * @return - equaling result
     * @throws IllegalAccessException
     */
    public boolean equalObjects(Object object1, Object object2) throws IllegalAccessException {
        if(object1 == null || object2 == null)
            return false;
        if(object1.getClass() != object2.getClass())
            return false;

        boolean result = true;
        Field[] fields = object1.getClass().getDeclaredFields();
        if(fields != null){
            for(Field field: fields){
                if(field.isAnnotationPresent(Equal.class)){
                    field.setAccessible(true);
                    if(!this.equalField(field, object1, object2)){
                        result = false;
                        break;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Equal object field references or values depends on annotation
     * @param field - field to equal
     * @param object1 - first object to equal
     * @param object2 - second object to equal
     * @return - field references equaling result
     * @throws IllegalAccessException
     */
    private boolean equalField(Field field, Object object1, Object object2) throws IllegalAccessException{
        boolean result = false;
        switch (field.getAnnotation(Equal.class).compare()){
            case VALUE:{
                return field.get(object1).equals(field.get(object2));
            }
            case REFERENCE:{
                return field.get(object1) == field.get(object2);
            }
        }
        return result;
    }


}
