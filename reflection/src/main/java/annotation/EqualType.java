package annotation;

/**
 * Enum for Equal annotation
 */
public enum EqualType {
    REFERENCE, VALUE;
}
