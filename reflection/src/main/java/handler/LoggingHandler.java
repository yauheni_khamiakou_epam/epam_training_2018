package handler;

import java.util.logging.Level;
import java.util.logging.Logger;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Simple handler with logging logic
 */
public class LoggingHandler implements InvocationHandler {
    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(LoggingHandler.class.getName());
    /**
     * Fields
     */
    private Object object;

    /**
     * Args constructor
     * @param object to set
     */
    public LoggingHandler(Object object) {
        this.object = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        LOGGER.log(Level.INFO, method.getName());
        return method.invoke(this.object, args);
    }
}
