package jUnitTests;

import bean.Cat;
import bean.Dog;
import bean.IAnimal;
import factory.ProxyFactory;
import org.junit.Assert;
import org.junit.Test;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tests for proxy annotation
 */
public class ProxyTest {
    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(ProxyTest.class.getName());

    @Test
    public void testSetProxyName(){
        String nameToSet = "someName";
        try {
            LOGGER.log(Level.INFO, "Testing proxy setName()");
            IAnimal animal = (IAnimal) ProxyFactory.getInstanceOf(Dog.class);
            animal.setName(nameToSet);
            Assert.assertEquals(nameToSet, animal.getName().get());
        } catch (ReflectiveOperationException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Test
    public void testRenameProxyName(){
        String nameToSet = "someName";
        String nameToRename = "someOtherName";
        try {
            LOGGER.log(Level.INFO, "Testing proxy rename()");
            IAnimal animal = (IAnimal) ProxyFactory.getInstanceOf(Dog.class);
            animal.setName(nameToSet);
            animal.rename(nameToRename);
            Assert.assertEquals(nameToRename, animal.getName().get());
        } catch (ReflectiveOperationException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Test
    public void testSetNoProxyName(){
        String nameToSet = "someName";
        try {
            LOGGER.log(Level.INFO, "Testing NO proxy setName()");
            IAnimal animal = (IAnimal) ProxyFactory.getInstanceOf(Cat.class);
            animal.setName(nameToSet);
            Assert.assertEquals(nameToSet, animal.getName().get());
        } catch (ReflectiveOperationException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Test
    public void testRenameNoProxyName(){
        String nameToSet = "someName";
        String nameToRename = "someOtherName";
        try {
            LOGGER.log(Level.INFO, "Testing NO proxy rename()");
            IAnimal animal = (IAnimal) ProxyFactory.getInstanceOf(Cat.class);
            animal.setName(nameToSet);
            animal.rename(nameToRename);
            Assert.assertEquals(nameToRename, animal.getName().get());
        } catch (ReflectiveOperationException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
