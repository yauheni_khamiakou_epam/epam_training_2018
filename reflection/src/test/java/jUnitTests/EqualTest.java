package jUnitTests;

import bean.Cat;
import equal.EqualAnalyzer;
import org.junit.Assert;
import org.junit.Test;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tests for equal annotation
 */
public class EqualTest {
    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(EqualTest.class.getName());

    @Test
    public void testEqualValueTrue(){
        Cat firstCat = new Cat("Cat", 10);
        Cat secondCat = new Cat("Cat", 10);
        try {
            Assert.assertEquals(true, new EqualAnalyzer().equalObjects(firstCat, secondCat));
        } catch (IllegalAccessException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Test
    public void testEqualNameValueFalse(){
        Cat firstCat = new Cat("Cat1", 10);
        Cat secondCat = new Cat("Cat2", 10);
        try {
            Assert.assertEquals(false, new EqualAnalyzer().equalObjects(firstCat, secondCat));
        } catch (IllegalAccessException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Test
    public void testEqualAgeValueFalse(){
        Cat firstCat = new Cat("Cat", 11);
        Cat secondCat = new Cat("Cat", 13);
        try {
            Assert.assertEquals(false, new EqualAnalyzer().equalObjects(firstCat, secondCat));
        } catch (IllegalAccessException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Test
    public void testEqualRefFalse(){
        Cat firstCat = new Cat(new String("Cat"), 10);
        Cat secondCat = new Cat(new String("Cat"), 10);
        try {
            Assert.assertEquals(false, new EqualAnalyzer().equalObjects(firstCat, secondCat));
        } catch (IllegalAccessException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
