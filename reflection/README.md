Reflection task.

Consists of two annotations: Equal and Proxy.
Factory for proxy annotation: ProxyFactory class.
Standart invocation handler: LoggingHandler. 
With opportunity to log bean method's name in runtime. 
Analyzer for equal annotation: EqualAnalyzer.

Build tool: Gradle. For testing simply run "gradle build" in package.