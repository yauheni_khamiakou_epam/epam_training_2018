Collections task.

Consists of two caches: CacheLRU, CacheLFU.
Common interface: ICache

Build tool: Ant.
For testing simply run "ant" in package.