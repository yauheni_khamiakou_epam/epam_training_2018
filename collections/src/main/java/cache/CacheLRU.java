package main.java.cache;

import java.util.HashMap;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * LRU cache implementation
 * @param <K> - element's key
 * @param <V> - element's value
 */
public class CacheLRU <K,V> implements ICache <K,V>{
    /**
     * Constants
     */
    private static final int DEFAULT_CAPACITY = 20;
    private static final char DELIMITER = '\n';
    /**
     * Fields
     */
    private int capacity;
    private int size;
    private volatile Node startNode;
    private volatile Node endNode;
    private volatile HashMap<K, Node> entryMap;
    private Lock lock;

    /**
     * Inner node class
     */
    class Node{
        Node prev;
        Node next;
        K key;
        V value;

        public Node(K key, V value){
            this.key = key;
            this.value = value;
        }
    }

    /**
     * No-args constructor
     */
    public CacheLRU() {
        this(CacheLRU.DEFAULT_CAPACITY);
    }

    /**
     * Constructor with capacity args
     * @param capacity - cache capacity
     */
    public CacheLRU(int capacity) {
        this.capacity = capacity;
        this.size = 0;
        this.entryMap = new HashMap<>();
        this.lock = new ReentrantLock();
    }

    @Override
    public Optional get(K key) {
        if (this.lock.tryLock()) {
            try {
                if (this.entryMap.containsKey(key)) {
                    Node node = this.entryMap.get(key);
                    this.reorderToFront(node);
                    return Optional.ofNullable(node.value);
                }
            } finally{
                this.lock.unlock();
            }
        }
        return Optional.empty();
    }

    @Override
    public void put(K key, V value) {
        if(value != null && key!= null && this.lock.tryLock()) {
            try {
                if (this.entryMap.containsKey(key)) {
                    Node node = this.entryMap.get(key);
                    node.value = value;
                    this.reorderToFront(node);
                } else {
                    Node node = new Node(key, value);
                    if (this.size < this.capacity) {
                        this.putToFront(node);
                        this.size++;
                    } else {
                        this.removeLastNodeFromEntry();
                        this.putToFront(node);
                    }
                }
            } finally {
                this.lock.unlock();
            }
        }
    }

    /**
     * Remove last node from entry map
     */
    private void removeLastNodeFromEntry(){
        if(this.endNode != null) {
            Node lastNode = this.entryMap.remove(this.endNode.key);
            this.endNode = lastNode.prev;
            if (this.endNode != null) {
                this.endNode.next = null;
            }
        }
    }

    /**
     * Add node to map front
     * @param node
     */
    private void putToFront(Node node){
        node.prev = null;
        node.next = this.startNode;
        if(this.startNode != null){
            this.startNode.prev = node;
        }
        this.startNode = node;
        if(this.endNode == null){
            this.endNode = node;
        }
        this.entryMap.put(node.key, node);
    }

    /**
     * Reorder node to map front
     * @param node - node to reorder
     */
    private void reorderToFront(Node node){
        Node next = node.next;
        Node prev = node.prev;
        if(prev == null){
            this.startNode = next;
        } else {
            prev.next = next;
        }
        if(next == null){
            this.endNode = prev;
        } else {
            next.prev = prev;
        }
        this.putToFront(node);
    }

    @Override
    public String toString() {
        Node node = this.startNode;
        StringBuilder stringResult = new StringBuilder("Capacity: ")
                .append(this.capacity)
                .append(" Current size: ")
                .append(this.size)
                .append(CacheLRU.DELIMITER);

        while(node != null){
            stringResult.append(node.key).append("--->").append(node.value).append(CacheLRU.DELIMITER);
            node = node.next;
        }
        return stringResult.toString();
    }
}
