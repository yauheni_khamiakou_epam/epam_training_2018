package main.java.cache;

import java.util.Optional;

/**
 * Cache interface
 * @param <K> - element's key
 * @param <V> - element's value
 */
public interface ICache <K,V>{

    /**
     * Get element by key
     * @param key - element's key
     * @return - element's value
     */
    public Optional get(K key);

    /**
     * Put element in cache
     * @param key - element's key
     * @param value - element's value
     */
    public void put(K key, V value);
}
