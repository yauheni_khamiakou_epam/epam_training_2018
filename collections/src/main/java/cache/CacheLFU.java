package main.java.cache;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * LFU cache implementation
 * @param <K> - element's key
 * @param <V> - element's value
 */
public class CacheLFU<K,V> implements ICache<K,V> {
    /**
     * Constants
     */
    private static final int DEFAULT_CAPACITY = 20;
    private static final float DEFAULT_EVICTION_FACTOR = 0.8f;
    private static final char DELIMITER = '\n';
    private static final int LOWEST_FREQUANCY = 1;
    /**
     * Fields
     */
    private int capacity;
    private float evictionFactor;
    private volatile HashMap<K, Node> entryMap;
    private volatile PriorityQueue<Node> priorityNodeQueue;
    private Lock lock;

    /**
     * Inner node class
     */
    class Node implements Comparable<Node>{
        K key;
        V value;
        int frequency;

        public Node(K key, V value){
            this.value = value;
            this.key = key;
            this.frequency = CacheLFU.LOWEST_FREQUANCY;
        }

        @Override
        public int compareTo(Node o) {
            return this.frequency - o.frequency;
        }
    }

    /**
     * No-args constructor
     */
    public CacheLFU() {
        this(CacheLFU.DEFAULT_CAPACITY, CacheLFU.DEFAULT_EVICTION_FACTOR);
    }

    /**
     * Constructor with capacity args
     * @param capacity - cache capacity
     * @param evictionFactor - clean factor
     */
    public CacheLFU(int capacity, float evictionFactor) {
        this.evictionFactor = evictionFactor;
        this.capacity = capacity;
        this.entryMap = new HashMap<>();
        this.priorityNodeQueue = new PriorityQueue<>();
        this.lock = new ReentrantLock();
    }

    /**
     * Is cache full
     * @return - full or not
     */
    public boolean isFull(){
        return this.priorityNodeQueue.size() >= this.capacity;
    }

    @Override
    public Optional get(K key) {
        if(this.lock.tryLock()) {
            try {
                Node node = this.entryMap.get(key);
                if (node != null) {
                    this.priorityNodeQueue.remove(node);
                    node.frequency++;
                    this.priorityNodeQueue.add(node);
                    return Optional.ofNullable(node.value);
                }
            } finally {
                this.lock.unlock();
            }
        }
        return Optional.empty();
    }

    @Override
    public void put(K key, V value) {
        if(value != null && key!= null && this.lock.tryLock()) {
            try {
                Node node = this.entryMap.get(key);
                if (node != null) {
                    node.value = value;
                    return;
                }
                if (this.isFull()) {
                    float entriesToRm = this.evictionFactor * this.capacity;
                    while (entriesToRm > 1) {
                        K keyToRemove = this.priorityNodeQueue.remove().key;
                        this.entryMap.remove(keyToRemove);
                        entriesToRm--;
                    }
                }
                node = new Node(key, value);
                this.priorityNodeQueue.add(node);
                this.entryMap.put(key, node);
            } finally {
                this.lock.unlock();
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder stringResult = new StringBuilder("Capacity: ")
                .append(this.capacity)
                .append(CacheLFU.DELIMITER);
        for(Map.Entry<K, Node> entry: this.entryMap.entrySet()){
            stringResult.append(entry.getKey()).append(": ")
                    .append(entry.getValue().frequency)
                    .append("--->")
                    .append(entry.getValue().value)
                    .append(CacheLFU.DELIMITER);
        }
        return stringResult.toString();
    }
}
