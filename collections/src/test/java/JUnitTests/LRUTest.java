package test.java.JUnitTests;

import main.java.cache.CacheLRU;
import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;
import java.util.logging.Logger;

public class LRUTest {
    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(LRUTest.class.getName());

    @Test
    public void testLRUGetAndPut() {
        CacheLRU<Integer, String> cache = new CacheLRU<>(5);
        cache.put(1,"a");
        cache.put(2,"b");
        cache.put(3,"c");
        cache.put(4,"d");
        Assert.assertEquals("c", cache.get(3).get());
    }

    @Test
    public void testCleanCache() {
        CacheLRU<Integer, String> cache = new CacheLRU<>(3);
        cache.put(1,"a");
        cache.put(2,"b");
        cache.put(3,"c");

        cache.get(3);
        cache.get(2);

        cache.put(5, "mm");
        Assert.assertEquals(Optional.empty(), cache.get(1));
    }

    @Test
    public void testGetNull() {
        CacheLRU<Integer, String> cache = new CacheLRU<>(3);
        cache.put(1,"a");

        Assert.assertEquals(Optional.empty(), cache.get(null));
    }

    @Test
    public void testPutNull() {
        CacheLRU<Integer, String> cache = new CacheLRU<>(3);
        cache.put(1,null);

        Assert.assertEquals(Optional.ofNullable(null), cache.get(1));
    }

    @Test
    public void testPutNullKey() {
        CacheLRU<Integer, String> cache = new CacheLRU<>(3);
        cache.put(null,"notNull");

        Assert.assertEquals(Optional.ofNullable(null), cache.get(1));
    }

    @Test
    public void testThreads() throws InterruptedException {
        String value1 = "value1";
        String value2 = "value2";
        CacheLRU<Integer, String> cache = new CacheLRU<>(3);
        Thread thread1 = new Thread(()->{
            try {
                cache.put(1, value1);
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread thread2 = new Thread(()->{cache.put(1, value2);});
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        Assert.assertEquals(value2, cache.get(1).get());
    }
}
