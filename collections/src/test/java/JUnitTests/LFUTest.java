package test.java.JUnitTests;

import main.java.cache.CacheLFU;
import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class LFUTest {
    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(LFUTest.class.getName());

    @Test
    public void testLFUGetAndPut() {
        CacheLFU<Integer, String> cache = new CacheLFU<>(5, 0.8f);
        cache.put(1,"a");
        cache.put(2,"b");
        cache.put(3,"c");
        cache.put(4,"d");
        Assert.assertEquals("c", cache.get(3).get());
    }

    @Test
    public void testCleanCache() {
        CacheLFU<Integer, String> cache = new CacheLFU<>(3, 0.5f);
        cache.put(1,"a");
        cache.put(2,"b");
        cache.put(3,"c");

        cache.get(3);
        cache.get(2);

        cache.put(5, "mm");
        Assert.assertEquals(Optional.empty(), cache.get(1));
    }

    @Test
    public void testGetNull() {
        CacheLFU<Integer, String> cache = new CacheLFU<>(3, 0.5f);
        cache.put(1,"a");

        Assert.assertEquals(Optional.empty(), cache.get(null));
    }

    @Test
    public void testPutNull() {
        CacheLFU<Integer, String> cache = new CacheLFU<>(3, 0.5f);
        cache.put(1,null);

        Assert.assertEquals(Optional.ofNullable(null), cache.get(1));
    }

    @Test
    public void testPutNullKey() {
        CacheLFU<Integer, String> cache = new CacheLFU<>(3, 0.5f);
        cache.put(null,"notNull");

        Assert.assertEquals(Optional.ofNullable(null), cache.get(1));
    }

    @Test
    public void testThreads() throws InterruptedException {
        String value1 = "value1";
        String value2 = "value2";
        CacheLFU<Integer, String> cache = new CacheLFU<>(4, 0.5f);
        Thread thread1 = new Thread(()->{
            try {
                cache.put(1, value1);
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread thread2 = new Thread(()->{cache.put(1, value2);});
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        Assert.assertEquals(value2, cache.get(1).get());
    }
}
