//package main;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.apache.tools.ant.Target;
import org.apache.tools.ant.Task;

/**
 * Custom target class. Validate build.xml ant file.
 */
public class BuildXmlValidator extends Task {

    /**
     * Constants
     */
    private static final String EMPTY_STRING = "";
    private static final String TRUE_FLAG = "true";

    /**
     * Regex
     */
    private static final String NAME_REGEX = "([a-zA-Z]*-*)*";


    /**
     * Info messages
     */
    private static final String MESSAGE_VALID_FILE = " is valid";
    private static final String MESSAGE_INVALID_FILE = " is invalid";

    /**
     * Fields
     */
    private String checkDepends;
    private String checkDefault;
    private String checkNames;

    /**
     * No-args constructor
     */
    public BuildXmlValidator() {
    }

    /**
     * Constructor with args
     * @param checkDepends - check depends in targets or not
     * @param checkDefault - check default in project or not
     * @param checkNames - check names in project or not
     */
    public BuildXmlValidator(final String checkDepends, final String checkDefault, final String checkNames) {
        this.checkDepends = checkDepends;
        this.checkDefault = checkDefault;
        this.checkNames = checkNames;
    }

    /**
     * @return checkDepends to get
     */
    public String getCheckDepends() {
        return checkDepends;
    }

    /**
     * @param checkDepends to set
     */
    public void setCheckDepends(final String checkDepends) {
        this.checkDepends = checkDepends;
    }

    /**
     * @return checkDefault to get
     */
    public String getCheckDefault() {
        return checkDefault;
    }

    /**
     * @param checkDefault to set
     */
    public void setCheckDefault(final String checkDefault) {
        this.checkDefault = checkDefault;
    }

    /**
     * @return checkNames to get
     */
    public String getCheckNames() {
        return checkNames;
    }

    /**
     * @param checkNames to set
     */
    public void setCheckNames(final String checkNames) {
        this.checkNames = checkNames;
    }

    /**
     * Collection of inner class beans
     */
    private List<BuildFile> buildFiles = new ArrayList<>();

    /**
     * Create and return BuildFile instance
     * @return - BuildFile instance
     */
    public BuildFile createBuildFile() {
        BuildFile buildFile = new BuildFile();
        buildFiles.add(buildFile);
        return buildFile;
    }

    /**
     * Build.xml file inner class
     */
    public class BuildFile {

        /**
         * Location field
         */
        private String location;

        /**
         * @return location to get
         */
        public String getLocation() {
            return location;
        }

        /**
         * @param location to set
         */
        public void setLocation(final String location) {
            this.location = location;
        }
    }

    /**
     * Check whether default target exists
     * @param project - project to check
     * @return - whether exists
     */
    private boolean isDefaultValid(final Project project){
        boolean result = false;
        String defaultTarget = project.getDefaultTarget();
        if (defaultTarget != null && !EMPTY_STRING.equals(defaultTarget)) {
            result = true;
        }
        return result;
    }

    /**
     * Check whether depends in target exists
     * @param project - project to check
     * @return - whether exists
     */
    private boolean isDependsValid(final Project project){
        boolean result = true;
        Enumeration<Target> targetEnumeration = project.getTargets().elements();
        List<Target> targetList = Collections.list(targetEnumeration);
        if (!targetList.isEmpty()){
            for (int i = 0; i < targetList.size() - 1; i++){
                if (!targetList.get(i).getDependencies().hasMoreElements()){
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Check whether target names in project is valid
     * @param project - project to check
     * @return - whether valid
     */
    private boolean isNamesValid(final Project project){
        Pattern pattern = Pattern.compile(NAME_REGEX);
        Enumeration<Target> targets = project.getTargets().elements();
        while (targets.hasMoreElements()){
            Matcher matcher = pattern.matcher(targets.nextElement().getName());
            if (!matcher.matches()){
                return false;
            }
        }
        return  true;
    }

    /**
     * Check document validation
     * @param fileName - document filename
     * @return - is valid or not
     */
    public boolean isValidXML(final String fileName) {
        try {
            Project project = new Project();
            ProjectHelper.configureProject(project, new File(fileName));
            boolean defaultValid = true;
            boolean dependsValid = true;
            boolean namesValid = true;

            if (TRUE_FLAG.equals(this.checkDefault)) {
                defaultValid = this.isDefaultValid(project);
            }
            if (TRUE_FLAG.equals(this.checkDepends)) {
                dependsValid = this.isDependsValid(project);
            }
            if (TRUE_FLAG.equals(this.checkNames)) {
                namesValid = this.isNamesValid(project);
            }
            return defaultValid && dependsValid && namesValid;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public void execute() {
        for (BuildFile buildfile : this.buildFiles) {
            StringBuilder msg = new StringBuilder(buildfile.getLocation());
            String location = buildfile.getLocation();
            if (isValidXML(location)) {
                msg.append(MESSAGE_VALID_FILE);
            } else
                msg.append(MESSAGE_INVALID_FILE);
            this.log(msg.toString(), Project.MSG_INFO);
        }

    }
}
