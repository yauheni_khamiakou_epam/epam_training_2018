package main;

/**
 * Ant main java class
 */
public final class HelloWorld {
    /**
     * Hided constructor
     */
    private HelloWorld() {
    }

    /**
     * Hello world main
     * @param args - main args
     */
    public static void main(final String[] args) {
        System.out.println("Hello World!");
    }
}
