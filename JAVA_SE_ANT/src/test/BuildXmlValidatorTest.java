//package test;

//import main.BuildXmlValidator;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test for different build files
 */
public class BuildXmlValidatorTest {
    /**
     * Validator instance
     */
    private BuildXmlValidator validator = new BuildXmlValidator("true", "true", "true");

    /**
     * Files path
     */
    private static final String TEST_FILES_DIR = new StringBuilder(System.getProperty("user.dir")).append("\\src\\test\\testFiles\\").toString();
    private static final String BUILD0_PATH = new StringBuilder(TEST_FILES_DIR).append("build0.xml").toString();
    private static final String BUILD1_PATH = new StringBuilder(TEST_FILES_DIR).append("build1.xml").toString();
    private static final String BUILD2_PATH = new StringBuilder(TEST_FILES_DIR).append("build2.xml").toString();
    private static final String BUILD3_PATH = new StringBuilder(TEST_FILES_DIR).append("build3.xml").toString();

    /**
     * Test build0.xml
     */
    @Test
    public void testIsValidXML0() {
        Assert.assertFalse(this.validator.isValidXML(BUILD0_PATH));

    }


    /**
     * Test build1.xml
     */
    @Test
    public void testIsValidXML1() {
        Assert.assertFalse(this.validator.isValidXML(BUILD1_PATH));

    }

    /**
     * Test build1.xml
     */
    @Test
    public void testIsValidXML2() {
        Assert.assertFalse(this.validator.isValidXML(BUILD2_PATH));

    }

    /**
     * Test build2.xml
     */
    @Test
    public void testIsValidXML3() {
        Assert.assertFalse(this.validator.isValidXML(BUILD3_PATH));

    }

}
