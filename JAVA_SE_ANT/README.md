To check 1-4 articles of ant task simply use "ant" command in package JAVA_SE_ANT
or use "ant -buildfile build.xml".

To check second task part use command "ant -buildfile validatorBuild.xml getIvy" firstly to get Ivy for correct work.
Then use command "ant -buildfile validatorBuild.xml" to run validation.

Package "main":
"HelloWorld" - simple class for first part of the task.
"BuildXmlValidator" - class for custom task.

Package test:
"BuildXmlValidatorTest" - junit test for different build.xml cases